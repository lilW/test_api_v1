from rest_framework import serializers

class capital_serializer(serializers.Serializer):
    capital_city = serializers.CharField()
    capital_population = serializers.IntegerField()
    user_name = serializers.CharField(source='user_id.user_name')