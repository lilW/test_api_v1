from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import test_v1

from .serializers import capital_serializer

# Create your views here.

class GetCapitalInfoView(APIView):
    def get(self, request):
        queryset = test_v1.objects.all()
        serializer_for_queryset = capital_serializer(
            instance=queryset,
            many=True
        )
        return Response(serializer_for_queryset.data)