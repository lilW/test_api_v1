from django.apps import AppConfig


class TestApiV1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'test_api_v1'
