from django.db import models

# Create your models here.

class user_v1(models.Model):
    user_name = models.CharField(max_length=30)

class test_v1(models.Model):
    capital = models.CharField(max_length=30)
    capital_city = models.CharField(max_length=30)
    capital_population = models.IntegerField()
    user_id = models.ForeignKey(user_v1, on_delete=models.CASCADE)

