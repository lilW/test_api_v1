from django.urls import path

from . import views

urlpatterns = [
    # path("", views.index, name="index"),
    path('api/capitals/', views.GetCapitalInfoView.as_view()),

]